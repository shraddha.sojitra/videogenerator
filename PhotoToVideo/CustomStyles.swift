//
//  CustomStyles.swift
//  PhotoToVideo
//
//  Created by Shraddha Sojitra on 17/12/19.
//  Copyright © 2019 Shraddha Sojitra. All rights reserved.
//

import Foundation
import SwiftUI

struct GradientBackgroundStyle: ButtonStyle {
    
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .padding(8)
            .background(LinearGradient(gradient: Gradient(colors: [Color.orange, Color.blue]), startPoint: .leading, endPoint: .trailing))
            .cornerRadius(40)
            .foregroundColor(Color.white)
            .padding(5)
            .overlay(RoundedRectangle(cornerRadius: 40)
                .stroke(Color.orange, lineWidth: 3))
            .shadow(color: Color.orange, radius: 5, x: 0, y: 0)
            .frame(minWidth: nil, idealWidth: nil, maxWidth: nil, minHeight: nil, idealHeight: 70, maxHeight: 100, alignment: .trailing)
    }
}
