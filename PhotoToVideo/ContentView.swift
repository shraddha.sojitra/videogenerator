//
//  ContentView.swift
//  PhotoToVideo
//
//  Created by Shraddha Sojitra on 16/12/19.
//  Copyright © 2019 Shraddha Sojitra. All rights reserved.
//

import SwiftUI
import AVFoundation

struct MyImage: Identifiable {
    var id = UUID()
    var image: UIImage
}

struct ContentView: View {
    
    @State var showActionSheet = false
    @State var showCaptureImageView: Bool = false
    @State var selection: Int? = nil
    
    @State var images: [MyImage]? = []
    
    var actionSheet: ActionSheet {
        ActionSheet(title: Text("Select From"), message: nil, buttons: [.default(Text("Camera"), action: {
            self.showCaptureImageView.toggle()
        }), .default(Text("Photos"), action: {
            print("Photos clicked")
        }), .cancel()])
    }
    
    var body: some View {
        NavigationView {
            VStack {
                if (self.showCaptureImageView) {
                    CaptureImageView(isShown: self.$showCaptureImageView, images: self.$images)
                } else {
                    HStack(alignment: VerticalAlignment.center, spacing: 30) {
                        Button(action: {
                            self.showActionSheet = true
                        }) {
                            ZStack {
                                HStack {
                                    Image("icn_Add")
                                    Text("Select Images")
                                }
                                .actionSheet(isPresented: $showActionSheet, content: {
                                    self.actionSheet
                                })
                            }
                        }
                        .buttonStyle(GradientBackgroundStyle())
                        
                        NavigationLink(destination: VideoPlayerView(images: images), tag: 1, selection: $selection) {
                            Button(action: {
                                self.selection = 1
                            }) {
                                ZStack {
                                    HStack {
                                        Image("icn_Add")
                                        Text("Make Video")
                                    }
                                }
                            }
                            .buttonStyle(GradientBackgroundStyle())
                        }
                    }
                }
                
                List(images!) { imageObj in
                    Image(uiImage: imageObj.image).resizable().scaledToFit()
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

