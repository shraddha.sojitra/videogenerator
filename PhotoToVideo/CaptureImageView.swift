//
//  CaptureImageView.swift
//  PhotoToVideo
//
//  Created by Shraddha Sojitra on 16/12/19.
//  Copyright © 2019 Shraddha Sojitra. All rights reserved.
//

import Foundation
import SwiftUI

struct CaptureImageView {
  
    @Binding var isShown: Bool
    @Binding var images: [MyImage]?
    
    func makeCoordinator() -> Coordinator {
      return Coordinator(self)
    }
}

extension CaptureImageView: UIViewControllerRepresentable {
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<CaptureImageView>) -> UIImagePickerController {
        let picker = UIImagePickerController()
        picker.delegate = context.coordinator
        picker.sourceType = .photoLibrary
        return picker
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController,
                                context: UIViewControllerRepresentableContext<CaptureImageView>) {
        
    }
}
